﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ottepel
{
    class Program
    {
        static void Main(string[] args)
        {
            int days, temperatures;
            int i = 0;
            int j = 0;
            int max = i;
            int first, sum;
            int second = 0;

            Console.Write("Input days: ");
            days = int.Parse(Console.ReadLine());
            if (days <= 0 || days > 100) { Console.WriteLine("Error."); }

            Console.WriteLine("Input temperatures: ");

            do
            {
                temperatures = int.Parse(Console.ReadLine());
                if (temperatures < -50 || temperatures > 50) { Console.WriteLine("Error."); }
                first = temperatures;

                sum = first + second;

                if (sum > second && first > 0)
                {
                    i++;
                    if(max <= i) {max = i;}
                }
                else
                {
                    i = 0;
                }

                second = first;
                j++;

            } while (j < days);
            

            Console.WriteLine($"Оттепель длилась {max} дней");

            Console.ReadKey();



        }
    }
}
